package com.jay.companymanager.Controller;

import com.jay.companymanager.model.PeopleItem;
import com.jay.companymanager.model.PeopleRquest;
import com.jay.companymanager.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/people")
public class PeopleController {
    private final PeopleService peopleService;
@PostMapping("/data")
    public String setPeople(@RequestBody PeopleRquest request){
        peopleService.setPeople(request.getName(),request.getPhone(),request.getBirthday());
        return "ok";
    }
    @GetMapping("/peoples")
    public List<PeopleItem> gatPeoples(){
    List<PeopleItem> result = peopleService.getPeoples();
    return result;
    }
}
