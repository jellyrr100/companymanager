package com.jay.companymanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PeopleRquest {
    private String name;
    private String phone;
    private LocalDate birthday;
}
