package com.jay.companymanager.repository;

import com.jay.companymanager.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {
}
