package com.jay.companymanager.service;

import com.jay.companymanager.entity.People;
import com.jay.companymanager.model.PeopleItem;
import com.jay.companymanager.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {
    private final PeopleRepository peopleRepository;

    public void setPeople(String name, String phone, LocalDate birthday) {
        People addData = new People();
        addData. setName(name);
        addData. setPhone(phone);
        addData. setBirthday(birthday);

        peopleRepository.save(addData);
    }

    public List<PeopleItem> getPeoples() {
        List<PeopleItem> result = new LinkedList<>();

        List<People> originData = peopleRepository.findAll();

        for (People item : originData) {
            PeopleItem addItem = new PeopleItem();
            addItem.setName(item.getName());
            addItem.setPhone(item.getPhone());

            result.add(addItem);
        }
        return result;
    }
}

